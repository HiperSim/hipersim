# Hipersim

## Introduction
Hipersim is a collection of tools for computationally-efficient simulation of Wind Energy problems using statistical and machine learning methods. Currently, the package contains the following submodules:

- **turbgen**: 
    - Generation of 3D frozen turbulence fields ("turbulence boxes") with the Mann turbulence spectrum
    - Constrained turbulence field generation and application of constraints on pre-generated turbulence fields

- **surrogates**:
    - A feedforward neural network toolbox, training and evaluation of feedforward neural networks. 
	- A set of tools for surrogate-based modeling and analysis of wind farms

## Links

- [Online documentation of new interface](https://hipersim.pages.windenergy.dtu.dk/hipersim/)
- [Manual describing old interface + theory references]( https://gitlab.windenergy.dtu.dk/HiperSim/hipersim/-/blob/master/doc/Turbgen_Manual_0.0.24.pdf?ref_type=heads)
- [MIT License](https://gitlab.windenergy.dtu.dk/TOPFARM/PyWake/blob/master/LICENSE)


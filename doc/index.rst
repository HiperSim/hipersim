.. Hipersim documentation master file, created by
   sphinx-quickstart on Wed Mar  6 10:58:50 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Hipersim's documentation!
====================================

Hipersim is a mann turbulence field generation and constrained turbulence simulation package

The first version of the package was implemented by Nikolay Dimitrov 30/11/2021.

Source code repository and issue tracker:
    https://gitlab.windenergy.dtu.dk/HiperSim/hipersim
    
Online documentation of new interface
    https://hipersim.pages.windenergy.dtu.dk/hipersim/

Manual describing old interface + theory references:
    https://gitlab.windenergy.dtu.dk/HiperSim/hipersim/-/blob/master/doc/Turbgen_Manual_0.0.24.pdf?ref_type=heads

License:
    MIT_

.. _MIT: https://gitlab.windenergy.dtu.dk/TOPFARM/PyWake/blob/master/LICENSE


Contents
==================

.. toctree::
    :maxdepth: 3
    
    QuickStart
    MannTurbulenceField
    SpeedupSimulation
    MannSpectrum
    Validation

